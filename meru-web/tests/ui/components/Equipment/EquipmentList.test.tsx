import { render } from '@tests/ui/testUtils'

import EquipmentList from '@components/Equipment/EquipmentList'
import { Equipment } from '@lib/equipment/Equipment'
import { Usage } from '@lib/equipment/Usage'

const SOME_EQUIPMENTS = [
  new Equipment(
    '456',
    'Intense Tracer 279',
    'https://picsum.photos/id/456/256',
    new Usage(99, 5.75)
  ),
]

describe('<EquipmentList />', () => {
  it('Should render the equipment list', () => {
    const { asFragment } = render(<EquipmentList equipments={SOME_EQUIPMENTS} />)

    expect(asFragment()).toMatchSnapshot()
  })
})
