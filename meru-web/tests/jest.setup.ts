import 'regenerator-runtime/runtime'
import 'isomorphic-fetch'

const mockHistoryPush = jest.fn()

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useLocation: () => ({ pathname: '' }),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
  useParams: jest.fn(() => ({})),
  useRouteMatch: () => ({ path: 'path', url: 'url' }),
}))

jest.mock('@components/Layout/Link', () => ({
  __esModule: true,
  default: 'div',
}))
