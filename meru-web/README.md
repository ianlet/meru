# MERU WEB

The web platform of Meru uses partly Firebase to run.

You can either run a local development version that completely bypasses Firebase :
```shell
yarn start
```

or you can simulate that you are running the production environment with Firebase Emulators :
```shell
yarn start:fake-prod
```