export enum QuickActionType {
  TRACK_COMPONENT = 'track-component',
  RECORD_USAGE = 'record-usage',
  SERVICE_COMPONENT = 'service-component',
  REGISTER_COMPONENT = 'register-component',
}
