export enum ServiceType {
  CHECK = 'check',
  REPLACEMENT = 'replacement',
}

export default ServiceType
