export class User {
  constructor(
    private _uid: string,
    private _name: string,
    private _email: string,
    private _avatarUrl: string
  ) {}

  get uid(): string {
    return this._uid
  }

  get name(): string {
    return this._name
  }

  get email(): string {
    return this._email
  }

  get avatarUrl(): string {
    return this._avatarUrl
  }
}
