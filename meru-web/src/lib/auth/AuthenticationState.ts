export enum AuthenticationState {
  PENDING = 'auth-state-pending',
  AUTHENTICATED = 'auth-state-authenticated',
  UNAUTHENTICATED = 'auth-state-unauthenticated',
}
