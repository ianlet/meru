class NotFoundError extends Error {
  constructor(message) {
    super(message || 'Resource not found')

    this.name = 'NotFoundError'
  }
}

export default NotFoundError
