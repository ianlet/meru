import NotFoundError from '/src/lib/NotFoundError'

class ComponentNotFoundError extends NotFoundError {
  constructor(componentId) {
    super(`Component with id ${componentId} not found`)

    this.name = 'ComponentNotFoundError'
    this.componentId = componentId
  }
}

export default ComponentNotFoundError
