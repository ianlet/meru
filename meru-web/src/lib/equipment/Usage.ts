export class Usage {
  constructor(private _distance: number, private _timeInHour: number) {}

  get distance(): number {
    return this._distance
  }

  get time(): number {
    return this._timeInHour
  }
}
