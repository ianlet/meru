import React from 'react'
import PropTypes from 'prop-types'

import Async from '/src/components/Layout/Async'
import Loading from '/src/components/Layout/Loading'
import ComponentCard from '/src/components/Component/ComponentCard'

import Services from '/src/services'

const ComponentCardContainer = ({ id }) => {
  const SmallLoading = <Loading size="medium" inline />

  const request = () => Services.components.findById(id).then((component) => ({ component }))

  return (
    <Async
      loading={SmallLoading}
      request={request}
      success={ComponentCard}
      data-test-id="component-card"
    />
  )
}

ComponentCardContainer.propTypes = {
  id: PropTypes.string.isRequired,
}

export default ComponentCardContainer
