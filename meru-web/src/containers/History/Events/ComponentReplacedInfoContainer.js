import React from 'react'

import Async from '/src/components/Layout/Async'
import Loading from '/src/components/Layout/Loading'
import ComponentReplacedInfo from '/src/components/History/Events/ComponentReplacedInfo'

import Services from '/src/services'

const ComponentReplacedInfoContainer = ({ component }) => {
  const SmallLoading = <Loading size="medium" inline />

  const request = () =>
    Services.components.findById(component.id).then((component) => ({ component }))

  return (
    <Async
      loading={SmallLoading}
      request={request}
      success={ComponentReplacedInfo}
      data-test-id="info"
    />
  )
}

export default ComponentReplacedInfoContainer
