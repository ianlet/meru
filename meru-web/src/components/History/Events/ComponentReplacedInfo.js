import React from 'react'

import Typography from '@mui/material/Typography'

const ComponentReplacedInfo = ({ component }) => <Typography>{component.name} replaced</Typography>

export default ComponentReplacedInfo
