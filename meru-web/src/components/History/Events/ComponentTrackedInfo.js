import React from 'react'

import Typography from '@mui/material/Typography'

const ComponentTrackedInfo = ({ component }) => <Typography>{component.name} tracked</Typography>

export default ComponentTrackedInfo
