import React from 'react'

import Typography from '@mui/material/Typography'

const ComponentCheckedInfo = ({ component }) => (
  <Typography>{component.name} checked & cleaned</Typography>
)

export default ComponentCheckedInfo
