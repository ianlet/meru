import React from 'react'

import Typography from '@mui/material/Typography'

const ComponentRegisteredInfo = ({ component }) => (
  <Typography>{component.name} registered</Typography>
)

export default ComponentRegisteredInfo
