import React from 'react'

import LoopIcon from '@mui/icons-material/Loop'

const ReplaceIcon = (props) => <LoopIcon {...props} />

export default ReplaceIcon
