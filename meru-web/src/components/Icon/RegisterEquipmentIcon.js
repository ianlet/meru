import React from 'react'

import FactCheckIcon from '@mui/icons-material/FactCheck'

const RegisterEquipmentIcon = (props) => <FactCheckIcon {...props} />

export default RegisterEquipmentIcon
