import React from 'react'

import FactCheckIcon from '@mui/icons-material/FactCheck'

const ServiceIcon = (props) => <FactCheckIcon {...props} />

export default ServiceIcon
