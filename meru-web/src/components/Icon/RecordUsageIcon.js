import React from 'react'

import AddChartIcon from '@mui/icons-material/Addchart'

const RecordUsageIcon = (props) => <AddChartIcon {...props} />

export default RecordUsageIcon
