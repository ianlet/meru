import React from 'react'

import TextField from '@mui/material/TextField'
import InputAdornment from '@mui/material/InputAdornment'

const TimeField = React.forwardRef(({ invalid, error, ...props }, ref) => (
  <TextField
    {...props}
    label="Time"
    type="number"
    InputProps={{
      endAdornment: <InputAdornment position="end">hours</InputAdornment>,
    }}
    error={invalid}
    helperText={error}
    ref={ref}
    inputRef={props.ref}
  />
))

export default TimeField
