import DevConfig from '@services/config/dev'
import TestConfig from '@services/config/test'
import ProdConfig from '@services/config/prod'

// TODO: Use Parcel 2 glob resolver

const config = { development: DevConfig, production: ProdConfig, test: TestConfig }

const configureServices = () => {
  const env = process.env.MERU_ENV || process.env.NODE_ENV || 'development'
  return config[env].configure()
}

const Services = configureServices()

export const runMigrations = async (services) => {
  console.log('Running migrations bitches...')

  const equipments = await services.equipments.all()

  equipments.forEach((e) => {
    e.components.forEach(async (c) => {
      try {
        await services.components.register({
          ...c,
          owner_id: e.owner_id,
          time: c.usage.time,
          distance: c.usage.distance,
        })
      } catch (error) {
        console.log(error)
      }
    })
  })
}

export default Services
