import FirestoreError from '/src/services/firestore/FirestoreError'
import ComponentNotFoundError from '/src/lib/component/ComponentNotFoundError'

class ComponentFirestore {
  constructor(firestore, auth, { emulatorHost, emulatorPort }) {
    this.firestore = firestore
    this.auth = auth

    if (emulatorHost) {
      this.firestore.useEmulator(emulatorHost, emulatorPort)
    }

    this.components = this.firestore.collection('components')
  }

  getComponents() {
    return this.components
      .where('owner_id', '==', this.auth.currentUser.uid)
      .get()
      .then((query) => {
        const e = []
        query.forEach((doc) => e.push(this._mapComponent(doc.data())))
        return e
      })
  }

  getComponent(componentId) {
    return this.components
      .doc(componentId)
      .get()
      .then((doc) => this._mapComponent(doc.data()))
      .catch((err) => {
        if (err.code === FirestoreError.PERMISSION_DENIED) {
          throw new ComponentNotFoundError(componentId)
        }

        throw err
      })
  }

  setComponent(component) {
    console.log('auth', this.auth.currentUser.uid)
    return this.components.doc(component.id).set(component)
  }

  _mapComponent(component) {
    const historyEvents = component.history.events.reduce(
      (events, event) => [...events, { ...event, date: event.date.toDate() }],
      []
    )

    return { ...component, history: { ...component.history, events: historyEvents } }
  }
}

export default ComponentFirestore
