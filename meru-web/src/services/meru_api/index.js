import DummyService from './dummy'

class MeruApi {
  constructor(options) {
    this.basePath = `${options.baseUrl}:${options.port}`

    this.dummies = new DummyService(this)
  }

  async get(path) {
    const url = this.formatUrl(path)
    const headers = this.generateHeaders('GET')
    const res = await fetch(url, { headers })
    return await res.json()
  }

  formatUrl(path) {
    return `${this.basePath}/${path}`
  }

  generateHeaders(method) {
    return { Accept: 'application/json' }
  }
}

const configure = (options) => {
  return new MeruApi({ baseUrl: 'http://localhost', port: 8080, ...options })
}

export default { configure }
